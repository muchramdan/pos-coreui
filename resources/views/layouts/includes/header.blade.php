<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>SolMusic | HTML Template</title>
    <meta charset="UTF-8">
    <meta name="description" content="SolMusic HTML Template">
    <meta name="keywords" content="music, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link href="{{asset ('app-assets/img/favicon.ico')}}" rel="shortcut icon"/>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset ('app-assets/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset ('app-assets/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset ('app-assets/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset ('app-assets/css/slicknav.min.css')}}"/>

    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="{{asset ('app-assets/css/style.css')}}"/>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
