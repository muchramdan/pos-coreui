-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2020 at 02:57 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos-coreui`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menulist`
--

CREATE TABLE `menulist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menulist`
--

INSERT INTO `menulist` (`id`, `name`) VALUES
(1, 'sidebar menu'),
(2, 'top menu');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `href`, `icon`, `slug`, `parent_id`, `menu_id`, `sequence`) VALUES
(1, 'Dashboard', '/', 'cil-speedometer', 'link', NULL, 1, 1),
(2, 'Login', '/login', 'cil-account-logout', 'link', NULL, 1, 2),
(3, 'Register', '/register', 'cil-account-logout', 'link', NULL, 1, 3),
(4, 'Theme', NULL, NULL, 'title', NULL, 1, 4),
(5, 'Colors', '/colors', 'cil-drop1', 'link', NULL, 1, 5),
(6, 'Typography', '/typography', 'cil-pencil', 'link', NULL, 1, 6),
(7, 'Base', NULL, 'cil-puzzle', 'dropdown', NULL, 1, 7),
(8, 'Breadcrumb', '/base/breadcrumb', NULL, 'link', 7, 1, 8),
(9, 'Cards', '/base/cards', NULL, 'link', 7, 1, 9),
(10, 'Carousel', '/base/carousel', NULL, 'link', 7, 1, 10),
(11, 'Collapse', '/base/collapse', NULL, 'link', 7, 1, 11),
(12, 'Forms', '/base/forms', NULL, 'link', 7, 1, 12),
(13, 'Jumbotron', '/base/jumbotron', NULL, 'link', 7, 1, 13),
(14, 'List group', '/base/list-group', NULL, 'link', 7, 1, 14),
(15, 'Navs', '/base/navs', NULL, 'link', 7, 1, 15),
(16, 'Pagination', '/base/pagination', NULL, 'link', 7, 1, 16),
(17, 'Popovers', '/base/popovers', NULL, 'link', 7, 1, 17),
(18, 'Progress', '/base/progress', NULL, 'link', 7, 1, 18),
(19, 'Scrollspy', '/base/scrollspy', NULL, 'link', 7, 1, 19),
(20, 'Switches', '/base/switches', NULL, 'link', 7, 1, 20),
(21, 'Tables', '/base/tables', NULL, 'link', 7, 1, 21),
(22, 'Tabs', '/base/tabs', NULL, 'link', 7, 1, 22),
(23, 'Tooltips', '/base/tooltips', NULL, 'link', 7, 1, 23),
(24, 'Buttons', NULL, 'cil-cursor', 'dropdown', NULL, 1, 24),
(25, 'Buttons', '/buttons/buttons', NULL, 'link', 24, 1, 25),
(26, 'Buttons Group', '/buttons/button-group', NULL, 'link', 24, 1, 26),
(27, 'Dropdowns', '/buttons/dropdowns', NULL, 'link', 24, 1, 27),
(28, 'Brand Buttons', '/buttons/brand-buttons', NULL, 'link', 24, 1, 28),
(29, 'Charts', '/charts', 'cil-chart-pie', 'link', NULL, 1, 29),
(30, 'Icons', NULL, 'cil-star', 'dropdown', NULL, 1, 30),
(31, 'CoreUI Icons', '/icon/coreui-icons', NULL, 'link', 30, 1, 31),
(32, 'Flags', '/icon/flags', NULL, 'link', 30, 1, 32),
(33, 'Brands', '/icon/brands', NULL, 'link', 30, 1, 33),
(34, 'Notifications', NULL, 'cil-bell', 'dropdown', NULL, 1, 34),
(35, 'Alerts', '/notifications/alerts', NULL, 'link', 34, 1, 35),
(36, 'Badge', '/notifications/badge', NULL, 'link', 34, 1, 36),
(37, 'Modals', '/notifications/modals', NULL, 'link', 34, 1, 37),
(38, 'Widgets', '/widgets', 'cil-calculator', 'link', NULL, 1, 38),
(39, 'Extras', NULL, NULL, 'title', NULL, 1, 39),
(40, 'Pages', NULL, 'cil-star', 'dropdown', NULL, 1, 40),
(41, 'Login', '/login', NULL, 'link', 40, 1, 41),
(42, 'Register', '/register', NULL, 'link', 40, 1, 42),
(43, 'Error 404', '/404', NULL, 'link', 40, 1, 43),
(44, 'Error 500', '/500', NULL, 'link', 40, 1, 44),
(45, 'Download CoreUI', 'https://coreui.io', 'cil-cloud-download', 'link', NULL, 1, 45),
(46, 'Try CoreUI PRO', 'https://coreui.io/pro/', 'cil-layers', 'link', NULL, 1, 46),
(47, 'Dashboard', '/', NULL, 'link', NULL, 2, 47),
(48, 'Notes', '/notes', NULL, 'link', NULL, 2, 48),
(49, 'Users', '/users', NULL, 'link', NULL, 2, 49),
(50, 'Settings', NULL, '', 'dropdown', NULL, 2, 50),
(51, 'Edit menu', '/menu/menu', NULL, 'link', 50, 2, 51),
(52, 'Edit menu elements', '/menu/element', NULL, 'link', 50, 2, 52),
(53, 'Edit roles', '/roles', NULL, 'link', 50, 2, 53);

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE `menu_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menus_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`id`, `role_name`, `menus_id`) VALUES
(1, 'guest', 1),
(2, 'user', 1),
(3, 'admin', 1),
(4, 'guest', 2),
(5, 'guest', 3),
(6, 'user', 4),
(7, 'admin', 4),
(8, 'user', 5),
(9, 'admin', 5),
(10, 'user', 6),
(11, 'admin', 6),
(12, 'user', 7),
(13, 'admin', 7),
(14, 'user', 8),
(15, 'admin', 8),
(16, 'user', 9),
(17, 'admin', 9),
(18, 'user', 10),
(19, 'admin', 10),
(20, 'user', 11),
(21, 'admin', 11),
(22, 'user', 12),
(23, 'admin', 12),
(24, 'user', 13),
(25, 'admin', 13),
(26, 'user', 14),
(27, 'admin', 14),
(28, 'user', 15),
(29, 'admin', 15),
(30, 'user', 16),
(31, 'admin', 16),
(32, 'user', 17),
(33, 'admin', 17),
(34, 'user', 18),
(35, 'admin', 18),
(36, 'user', 19),
(37, 'admin', 19),
(38, 'user', 20),
(39, 'admin', 20),
(40, 'user', 21),
(41, 'admin', 21),
(42, 'user', 22),
(43, 'admin', 22),
(44, 'user', 23),
(45, 'admin', 23),
(46, 'user', 24),
(47, 'admin', 24),
(48, 'user', 25),
(49, 'admin', 25),
(50, 'user', 26),
(51, 'admin', 26),
(52, 'user', 27),
(53, 'admin', 27),
(54, 'user', 28),
(55, 'admin', 28),
(56, 'user', 29),
(57, 'admin', 29),
(58, 'user', 30),
(59, 'admin', 30),
(60, 'user', 31),
(61, 'admin', 31),
(62, 'user', 32),
(63, 'admin', 32),
(64, 'user', 33),
(65, 'admin', 33),
(66, 'user', 34),
(67, 'admin', 34),
(68, 'user', 35),
(69, 'admin', 35),
(70, 'user', 36),
(71, 'admin', 36),
(72, 'user', 37),
(73, 'admin', 37),
(74, 'user', 38),
(75, 'admin', 38),
(76, 'user', 39),
(77, 'admin', 39),
(78, 'user', 40),
(79, 'admin', 40),
(80, 'user', 41),
(81, 'admin', 41),
(82, 'user', 42),
(83, 'admin', 42),
(84, 'user', 43),
(85, 'admin', 43),
(86, 'user', 44),
(87, 'admin', 44),
(88, 'guest', 45),
(89, 'user', 45),
(90, 'admin', 45),
(91, 'guest', 46),
(92, 'user', 46),
(93, 'admin', 46),
(94, 'guest', 47),
(95, 'user', 47),
(96, 'admin', 47),
(97, 'user', 48),
(98, 'admin', 48),
(99, 'admin', 49),
(100, 'admin', 50),
(101, 'admin', 51),
(102, 'admin', 52),
(103, 'admin', 53);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_11_085455_create_notes_table', 1),
(5, '2019_10_12_115248_create_status_table', 1),
(6, '2019_11_08_102827_create_menus_table', 1),
(7, '2019_11_13_092213_create_menurole_table', 1),
(8, '2019_12_10_092113_create_permission_tables', 1),
(9, '2019_12_11_091036_create_menulist_table', 1),
(10, '2019_12_18_092518_create_role_hierarchy_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 4),
(2, 'App\\User', 5),
(2, 'App\\User', 6),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(2, 'App\\User', 12);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applies_to_date` date NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `title`, `content`, `note_type`, `applies_to_date`, `users_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Ut et perferendis qui accusamus.', 'Dignissimos soluta natus voluptates. Dolores molestiae dicta repellat id sit. Optio repellat omnis quia nihil aliquid.', 'repellat', '2008-03-07', 7, 2, NULL, NULL),
(2, 'Alias laudantium repellat.', 'Qui dolores qui magnam exercitationem vel. Ipsa vero nihil quas ut ut aut dignissimos quod. Temporibus repellat voluptas voluptatum id neque rerum. Blanditiis quia beatae iste quam adipisci aut ad.', 'aut', '1999-12-25', 10, 4, NULL, NULL),
(3, 'Ut sit nihil omnis.', 'Tempora magnam labore soluta quasi dolores. Molestias iure velit officia in. Soluta aperiam et iusto exercitationem et.', 'quaerat esse', '1999-11-02', 7, 4, NULL, NULL),
(4, 'Sit totam voluptas.', 'Quos aut magni consequuntur delectus et sequi exercitationem earum. Nobis omnis et porro voluptatem qui id accusamus. Dolorem odit dolorem corporis et occaecati. Officiis quos dignissimos aliquam fugit.', 'eligendi', '1977-02-19', 10, 1, NULL, NULL),
(5, 'Provident corrupti et.', 'Nisi nobis vel consectetur et voluptatem facilis doloremque qui. Est tenetur mollitia quasi in placeat autem. Aut dolor et beatae consequatur quis. Facilis maxime distinctio natus eos inventore qui accusamus.', 'ipsam nihil', '1986-05-31', 3, 3, NULL, NULL),
(6, 'Voluptatibus in suscipit officia consequuntur.', 'Ducimus a expedita sit quia impedit sit vero. Voluptates cupiditate ut ut. Perferendis aut porro rem maxime repellendus. Dolorem nisi molestiae beatae iste eligendi consequatur hic. Ex beatae illum soluta eos porro itaque.', 'quo nulla', '1995-12-12', 7, 4, NULL, NULL),
(7, 'Suscipit dolore ab ducimus voluptas ipsam.', 'Quas qui numquam beatae provident mollitia quis. Dolor fuga fugiat dignissimos eaque doloribus culpa nostrum aut. Consequatur dignissimos tempora adipisci doloremque perspiciatis eveniet saepe.', 'hic', '1987-04-21', 6, 1, NULL, NULL),
(8, 'Et facilis dolores.', 'Minima et qui est id qui minima debitis. Asperiores voluptatem labore maxime nihil iste odit nesciunt. Explicabo nam officia pariatur et maxime ut illum. In blanditiis voluptatibus nobis officia iste laboriosam natus.', 'dolorem tempora', '1975-10-08', 2, 1, NULL, NULL),
(9, 'Est at est ea.', 'Molestiae aut accusamus quibusdam consequatur repudiandae. Explicabo optio labore pariatur recusandae. Excepturi ipsam quas cupiditate blanditiis. Aliquid blanditiis qui quam quis qui.', 'dolores', '1984-08-13', 4, 3, NULL, NULL),
(10, 'Quasi libero hic repellendus.', 'Ut ex voluptas laborum qui esse totam quia. Labore facere debitis officia quos aliquid doloremque mollitia. Ipsam aut aut minus ea pariatur voluptatem veniam fugit.', 'labore mollitia', '2006-07-13', 4, 1, NULL, NULL),
(11, 'Rerum nesciunt aut impedit veritatis.', 'Beatae commodi placeat ratione et. Occaecati ea ipsam aut magnam consequatur et rerum sunt. Tempora eligendi ullam doloremque dolorem dolor.', 'ut voluptatibus', '1974-10-26', 4, 3, NULL, NULL),
(12, 'Inventore perferendis quo nobis ratione rerum.', 'Nihil sequi cumque velit laboriosam commodi ea. Nesciunt eum quo nihil fugiat sit odit error. Aspernatur nihil cum saepe deserunt dicta possimus.', 'error', '2010-03-02', 11, 3, NULL, NULL),
(13, 'Porro eaque sunt.', 'Quasi pariatur ut dolor molestiae omnis sed. Vel rerum vel ipsam. Facere quia magnam quo sed cupiditate at eaque doloribus. Nostrum ut delectus laboriosam excepturi et enim.', 'magni', '2000-01-31', 2, 2, NULL, NULL),
(14, 'Quidem ut tempore sunt.', 'Pariatur sit sit incidunt repellendus illo ut dolorem. Dolorem quis pariatur nihil eveniet voluptate aspernatur provident.', 'voluptates', '1980-01-08', 10, 1, NULL, NULL),
(15, 'Dolores soluta excepturi laborum sequi.', 'Magni ad et possimus in numquam id quo. Magnam iusto autem deserunt consectetur alias placeat. Veniam sint vero recusandae quis dolorem.', 'tempora sit', '1975-07-08', 2, 1, NULL, NULL),
(16, 'Aut similique est.', 'Qui sunt quidem quam nisi dolor est a. Perferendis ab ea et et ea. Sunt nisi quis quasi rem et odit sit. Ipsum esse consequatur itaque ipsa omnis expedita.', 'possimus', '2005-07-04', 6, 2, NULL, NULL),
(17, 'Ipsa sint temporibus dolore quia quia.', 'Minima aut natus dignissimos animi velit voluptatem aliquid molestiae. Perspiciatis facere modi cum. Consequuntur laboriosam sint est voluptate provident aliquam dignissimos. Ipsum deserunt iusto eos voluptatem voluptas. Cupiditate molestiae blanditiis nesciunt similique omnis.', 'veniam voluptatem', '1997-09-09', 5, 3, NULL, NULL),
(18, 'Earum quidem placeat velit iure id.', 'Iusto autem impedit aut. Eligendi non non explicabo maxime. Tenetur et enim in odio. Quo maxime corporis dolores libero.', 'debitis', '1978-03-24', 3, 4, NULL, NULL),
(19, 'Qui voluptatem ut aspernatur.', 'Voluptas nihil minus doloribus dignissimos ab soluta. Vel rerum et sed tenetur sit et beatae. Neque ut harum nisi deserunt. Asperiores quia corporis necessitatibus esse voluptatem provident vitae.', 'corporis ab', '1984-06-26', 5, 2, NULL, NULL),
(20, 'Sint nemo consequatur nobis nisi.', 'Sunt praesentium asperiores illum quis aperiam. Qui aut omnis vel quidem. Est nesciunt distinctio eos unde minus pariatur.', 'laboriosam officia', '1973-02-02', 11, 4, NULL, NULL),
(21, 'Facilis eligendi enim sit.', 'Debitis alias qui aut iste. Quo voluptas beatae ea vitae illo ut. Ut nulla saepe rerum eaque molestiae nulla unde. Veniam ea unde et officiis placeat.', 'quis eum', '1987-02-06', 10, 3, NULL, NULL),
(22, 'Fugit dolor eveniet mollitia.', 'Nulla sint ut odit. Ut ipsa possimus veritatis omnis voluptas rem.', 'eaque vitae', '1986-05-19', 6, 1, NULL, NULL),
(23, 'Laboriosam et ex voluptatem sint quis.', 'Architecto rerum quos ducimus ad expedita esse. Fuga quam possimus sapiente repudiandae recusandae iusto veritatis itaque. Laborum ab velit fuga consequatur.', 'est', '1980-06-07', 9, 3, NULL, NULL),
(24, 'Expedita voluptas qui perspiciatis laudantium corrupti.', 'Itaque vitae saepe dicta et omnis impedit. Error ex ex nisi soluta. Sunt eligendi mollitia debitis consectetur tempore corrupti. Dignissimos ipsam similique repellat.', 'perspiciatis laboriosam', '2008-02-01', 5, 4, NULL, NULL),
(25, 'Nemo porro qui.', 'Quos doloribus mollitia qui veniam sit ut. Sit est repellendus quaerat dolores aut. Eveniet illum quis dolorem velit expedita. Esse similique provident quas repudiandae nisi mollitia doloremque molestias.', 'reiciendis et', '2019-05-22', 10, 3, NULL, NULL),
(26, 'Nihil blanditiis repellendus fuga quam explicabo.', 'Velit dolores aut accusamus iure quo non consequatur. Necessitatibus quia quibusdam id est qui voluptatum sed. Accusamus fugit et vero quia maiores alias. Consectetur alias magnam eum veniam.', 'qui', '2005-01-01', 10, 2, NULL, NULL),
(27, 'Nobis consectetur nam at soluta.', 'Quos quia ut vero vero natus id. Non expedita nihil deleniti sint et officiis doloribus ad. Delectus sunt eum mollitia ut facilis. Sit magni facilis aliquam sint.', 'nesciunt mollitia', '1993-11-19', 8, 3, NULL, NULL),
(28, 'Ut minima est ullam nisi.', 'Dicta delectus doloremque vel corrupti ducimus cumque doloremque. Eum minus aut excepturi consequatur mollitia. Deserunt qui reprehenderit nisi. Totam aut minus et necessitatibus et.', 'deserunt saepe', '1992-10-06', 7, 2, NULL, NULL),
(29, 'Ad soluta sit dolorem quaerat.', 'Qui et porro dolore suscipit officiis blanditiis quis. Aut voluptates quod quo ut esse non. Ducimus incidunt excepturi maiores est quaerat voluptatum. Odit qui aliquid soluta et odit ratione.', 'possimus aperiam', '1995-01-10', 4, 4, NULL, NULL),
(30, 'Tempora sequi sit ut hic.', 'Repellat natus harum quo. Consequuntur ratione nam et consectetur vero est beatae. Voluptatem vitae fuga facilis rerum error et et et.', 'pariatur', '1973-11-13', 2, 2, NULL, NULL),
(31, 'Vero esse et ab.', 'Fuga hic et voluptates libero id dicta sapiente. Doloribus laudantium sapiente animi eligendi deleniti debitis sunt id. Id quia sapiente atque officiis. Incidunt dolorem iste molestiae possimus sint.', 'et animi', '2014-11-10', 6, 2, NULL, NULL),
(32, 'Saepe in placeat.', 'Qui et placeat veniam aliquid minus. Veritatis voluptates doloribus aliquid asperiores odio atque. Eaque blanditiis quidem nobis ab voluptatem accusantium. Esse ut saepe fuga ducimus repudiandae eum.', 'quae voluptas', '1973-11-27', 5, 1, NULL, NULL),
(33, 'Nulla voluptatem velit quaerat.', 'Optio voluptatem consequatur asperiores eos alias. Explicabo omnis provident enim rerum labore ut. Nostrum beatae est nihil. Et ad suscipit dolores sunt nesciunt est mollitia omnis.', 'excepturi natus', '1985-07-21', 11, 3, NULL, NULL),
(34, 'Aperiam reprehenderit quibusdam saepe sed nam.', 'Voluptatem dolorem sint commodi a. Eaque aut cupiditate enim iste inventore et. Reprehenderit est sunt corporis facilis. Quia in fuga et dolor voluptas deleniti non est. Adipisci ut atque voluptatibus labore quisquam consequatur.', 'facere', '1977-10-16', 3, 1, NULL, NULL),
(35, 'Dolorum nostrum nihil est aut eum.', 'Est laboriosam a distinctio fuga. Ut ipsum veritatis autem quis aliquam autem quisquam. Doloribus et quod illum omnis tempora ullam. Omnis culpa distinctio est vel.', 'molestias', '2017-05-08', 11, 1, NULL, NULL),
(36, 'Asperiores aliquid ut quis.', 'Minus molestiae similique et porro pariatur eos excepturi omnis. Et quae autem in voluptas quibusdam inventore amet. Voluptatibus asperiores nemo illo id aut quam.', 'possimus', '2010-07-17', 4, 1, NULL, NULL),
(37, 'Consectetur fugiat consectetur est.', 'Et nemo sit sequi laboriosam. Ea nihil non nemo aperiam. Quia tempore culpa est molestias ab. Impedit cum et possimus vero. Quo accusamus autem optio ut.', 'consequuntur', '2012-11-13', 11, 4, NULL, NULL),
(38, 'Eum nobis quidem.', 'Eius et vitae autem esse doloribus iure libero. Doloribus est reprehenderit nam. Autem consequatur voluptas quod voluptatem veritatis. Sed porro id quidem id.', 'voluptatem eos', '1971-08-07', 4, 1, NULL, NULL),
(39, 'In qui tempore.', 'Architecto mollitia quasi vitae veniam temporibus nam perferendis. Voluptas vel sunt porro ullam voluptate. Ipsam minus officiis vel aut.', 'sit', '1985-05-22', 7, 2, NULL, NULL),
(40, 'Veritatis laboriosam voluptatem maiores.', 'Fugiat nihil praesentium qui sed dolorum voluptate commodi. Voluptatem in et eius. Necessitatibus earum eveniet et nihil vero.', 'numquam', '1990-01-03', 7, 3, NULL, NULL),
(41, 'Quis praesentium officiis.', 'Enim sunt recusandae deleniti inventore excepturi. Quaerat eaque dolorem voluptatem incidunt rerum. Maxime enim consequatur porro in sequi eum.', 'iusto', '1994-09-15', 8, 3, NULL, NULL),
(42, 'Itaque excepturi atque voluptatibus accusantium.', 'Qui cumque et ut esse. Et eligendi vel necessitatibus unde.', 'eum illo', '1990-06-27', 8, 1, NULL, NULL),
(43, 'Sed est esse a.', 'Animi quae quasi mollitia ut. Consequuntur dolorum fugiat nam repudiandae. Soluta laboriosam quis autem est accusamus. Et similique ab et architecto consequatur aspernatur quo est.', 'deleniti', '1972-10-12', 4, 2, NULL, NULL),
(44, 'Facere esse assumenda ut veniam.', 'Amet eos quis voluptate pariatur tenetur ullam. Pariatur corporis quidem non sit. Beatae explicabo veritatis quis rerum et et rem. Itaque inventore assumenda a voluptatem voluptatem.', 'dolorem', '2013-08-29', 3, 4, NULL, NULL),
(45, 'Ut odio aspernatur iste recusandae.', 'Quam culpa est a non cum ut quos. Veritatis quia soluta doloribus expedita ipsa officiis adipisci molestiae. Eius autem et asperiores ullam nihil quibusdam. Ullam dolor ea quo error necessitatibus quia iusto.', 'voluptatem', '1977-06-13', 11, 4, NULL, NULL),
(46, 'Dolore nemo dolor consequatur facilis.', 'Tempora illo dolores adipisci sunt molestiae. Maiores quasi tenetur omnis ipsam eveniet.', 'quidem', '1971-12-16', 8, 3, NULL, NULL),
(47, 'Distinctio est minus.', 'Quasi consequatur rerum doloribus asperiores veritatis facere autem voluptatem. Dignissimos cumque odio sunt minima. Commodi quos et consectetur quod molestiae quas. Molestiae sint optio harum provident molestiae exercitationem porro.', 'hic vel', '1999-02-04', 9, 3, NULL, NULL),
(48, 'Non maiores cum.', 'Ipsam quis optio blanditiis assumenda dolorem. Vero aspernatur corrupti ipsum commodi tempora quibusdam omnis voluptatum. Molestias delectus blanditiis dolor debitis.', 'commodi', '1974-01-09', 5, 2, NULL, NULL),
(49, 'Expedita excepturi perspiciatis est dolorem.', 'Sint et quibusdam pariatur dolorem. Quos consectetur eum consectetur ut expedita nihil. Dolores vero saepe in ut dolores. Autem officia molestiae eligendi nemo placeat optio.', 'facilis', '2002-03-06', 6, 1, NULL, NULL),
(50, 'Est quia totam sed.', 'Et rem velit architecto delectus facere excepturi mollitia dolor. At similique ut nulla ut. Unde vero cum vero voluptatum necessitatibus quae. Rerum quasi voluptatem repellendus ut assumenda.', 'voluptatum', '1970-11-12', 9, 2, NULL, NULL),
(51, 'Cupiditate tempore facilis harum ex.', 'Reprehenderit labore est tempora quia sed odit fuga. Ipsum laborum quia quos odit. Accusantium aut autem facilis delectus laborum. Et id nisi cum fugit impedit quia.', 'velit', '1987-06-19', 9, 3, NULL, NULL),
(52, 'Qui perferendis mollitia harum.', 'Quos eum est optio. Porro eos et autem minima. Consequuntur id sit et sint quis nesciunt et. Nihil nemo perspiciatis et enim enim numquam.', 'illo', '1998-11-06', 11, 1, NULL, NULL),
(53, 'Libero quo et.', 'Amet quia inventore maiores possimus. Cum consectetur beatae ut deserunt molestiae nulla. Ipsum dolores velit quia totam. Eos assumenda eligendi enim fugiat repellat cumque nam.', 'ipsum', '2015-01-24', 6, 1, NULL, NULL),
(54, 'Illo vero exercitationem quo omnis eum.', 'Delectus sint voluptatibus est odit. Vel architecto placeat corporis sint enim cupiditate voluptatem. Ut sit doloribus et illo suscipit et aliquid. Voluptatem voluptates aut qui qui.', 'nisi', '2003-08-07', 2, 3, NULL, NULL),
(55, 'Est saepe ipsam fuga dolor.', 'Dolorem similique odio numquam ea. Esse blanditiis inventore quisquam voluptas sapiente. Ipsa quidem eum iure necessitatibus.', 'ipsa dolores', '1972-08-12', 2, 3, NULL, NULL),
(56, 'Delectus nesciunt minima qui.', 'Eligendi qui dolor non possimus velit. Doloremque dolorem beatae consequatur aliquam ut. Id beatae odio quibusdam. Quia impedit ab laboriosam asperiores.', 'tenetur', '2014-10-24', 10, 2, NULL, NULL),
(57, 'Nesciunt et recusandae pariatur accusamus vero.', 'Ea saepe dolor ullam iste porro iure voluptates repellendus. Vitae delectus quo corporis provident. Eos possimus officiis sed qui. Enim et maiores rerum asperiores.', 'cumque', '1985-10-09', 9, 2, NULL, NULL),
(58, 'Molestiae quaerat ipsum inventore aut.', 'A iste cum laborum at possimus unde id fugit. Excepturi facilis tenetur rerum itaque officiis sit. Et hic blanditiis aut reiciendis vitae. Aut nam ut eius impedit.', 'numquam', '2019-06-30', 11, 2, NULL, NULL),
(60, 'Dolorem excepturi quia non soluta ut.', 'Officiis ut reiciendis amet. Et non unde qui qui at animi modi aliquid. Labore facere velit ut et.', 'et iste', '1992-01-27', 2, 3, NULL, NULL),
(61, 'Blanditiis cum similique.', 'Repellat suscipit eos soluta dicta. Ut inventore consectetur nulla blanditiis et mollitia aliquam id. Ea quidem est aspernatur ipsa eius fugit inventore. Enim autem dolores in autem.', 'ut', '2002-06-24', 4, 2, NULL, NULL),
(62, 'In et sit quisquam neque.', 'Molestias velit exercitationem soluta non ad quas. Facilis sed suscipit assumenda dicta at veniam cumque. Reiciendis dolorem est est aut eaque maxime.', 'enim', '1986-09-08', 2, 1, NULL, NULL),
(63, 'Laboriosam sed ratione.', 'Minus sunt voluptates aut voluptas. Minus sed distinctio sint adipisci et. Accusantium nemo doloribus quos.', 'consequuntur', '2008-05-11', 4, 4, NULL, NULL),
(64, 'Id doloremque quidem velit.', 'Vel sed voluptatem voluptates minus fuga vero. Harum minus modi quia ut quas. Consequatur enim occaecati voluptatem.', 'qui quam', '1974-01-29', 9, 1, NULL, NULL),
(65, 'Sint incidunt quia quis sit aperiam.', 'Quibusdam deleniti et praesentium voluptatem quis explicabo et. Rerum maxime aliquid provident minima quis deserunt qui. Omnis ut pariatur harum pariatur. Ut rerum tempora explicabo voluptatum autem enim.', 'delectus quo', '2004-04-29', 9, 3, NULL, NULL),
(66, 'Eveniet doloremque repudiandae et.', 'Magnam aliquam et consectetur eius. Asperiores non sit qui aliquid. Dolor est adipisci dolore laudantium. Veniam ab enim enim atque.', 'corporis tempore', '2018-12-21', 4, 4, NULL, NULL),
(67, 'Labore voluptas magni et odio.', 'Eaque voluptatibus est voluptas eveniet. Odit nisi est aut quae quos. Et nihil eius et corrupti nam sed voluptatem.', 'sint eos', '2017-02-28', 10, 3, NULL, NULL),
(68, 'Numquam aperiam velit.', 'Labore eveniet veritatis libero dignissimos. Voluptas ratione alias tempora voluptatem dolor veniam. Dolor sit cumque inventore laborum eligendi consequatur. Officiis itaque et quod et saepe.', 'quia rerum', '2014-12-16', 6, 3, NULL, NULL),
(69, 'Modi quos maxime inventore.', 'Tempora in ea minima vero labore delectus. Corporis amet quo et illum hic. Maiores et ut maiores ratione. Eligendi nam maxime vel modi molestias libero.', 'saepe', '2001-03-06', 11, 4, NULL, NULL),
(70, 'Reprehenderit aut dicta.', 'Sed ullam et nesciunt explicabo aut nisi accusamus harum. Non accusantium ut vel fugiat doloribus quia. Aliquid ut qui doloremque voluptatem.', 'fuga sed', '1982-09-04', 5, 3, NULL, NULL),
(71, 'Laudantium laborum aut nostrum.', 'Laboriosam culpa et natus qui fugiat magni iusto est. Similique voluptas et eum. Sequi dolores aperiam est. Numquam aliquam labore aut provident perferendis aperiam velit.', 'laborum qui', '1973-10-02', 4, 2, NULL, NULL),
(72, 'Soluta culpa laboriosam vero.', 'Voluptas qui laudantium atque omnis totam eum quae. Laboriosam magni sit sit totam culpa. Saepe doloremque officia a perspiciatis aut.', 'aperiam', '2002-08-08', 6, 4, NULL, NULL),
(73, 'Sed delectus eius aut doloremque.', 'Accusantium quia dolore eos et necessitatibus. Tenetur necessitatibus perspiciatis aut. Optio quo nemo est in iste consequatur. Modi nostrum sunt enim et nostrum porro.', 'cumque fuga', '1998-10-24', 5, 3, NULL, NULL),
(74, 'Nesciunt et nihil sint praesentium.', 'Magni velit tempore quis placeat fuga. Nihil nemo et totam ad odio nulla maxime. Dolor harum aut aut sunt excepturi accusantium. Quo rerum maiores ipsam nemo.', 'velit animi', '1981-08-01', 11, 3, NULL, NULL),
(75, 'Minima accusantium et aut saepe pariatur.', 'Sed aut nemo tempore ipsum voluptas velit. Asperiores veritatis et accusantium dicta. Magnam eligendi labore voluptatem numquam quo. Quis aliquid quod et.', 'modi ducimus', '1995-07-27', 4, 4, NULL, NULL),
(76, 'Nam dolorum veniam saepe.', 'Non dolores corrupti est dolor reprehenderit ad amet. Consequatur libero omnis et autem. Distinctio et inventore excepturi repudiandae perspiciatis.', 'ratione asperiores', '2019-11-20', 8, 3, NULL, NULL),
(77, 'Rerum laboriosam veritatis voluptatibus.', 'Quas animi eos distinctio ut exercitationem commodi. Impedit odio iure delectus harum. Et rem est quia.', 'odio aut', '2007-01-12', 5, 3, NULL, NULL),
(78, 'Ea repellendus rerum odio cupiditate.', 'Vel labore dicta autem ut rem. Earum expedita sed sed tempora et perferendis vitae. Accusantium et repellat occaecati.', 'dolore voluptatem', '2009-09-04', 2, 2, NULL, NULL),
(79, 'Deserunt aut accusantium autem dolor placeat.', 'Necessitatibus ea rerum qui placeat. Facilis libero omnis laborum mollitia non. Ex natus qui ea exercitationem eos.', 'repellendus', '1998-06-04', 5, 1, NULL, NULL),
(80, 'Voluptas omnis unde.', 'Fugit similique fuga culpa nihil et aut aperiam ut. Facilis nam voluptatum est sint.', 'magni aliquam', '1996-06-26', 9, 4, NULL, NULL),
(81, 'Reprehenderit officiis ea fuga.', 'Aliquam explicabo repudiandae velit. Enim molestias aliquid esse pariatur ea. Rerum doloribus commodi nulla placeat saepe provident necessitatibus. Veritatis et esse molestiae.', 'mollitia voluptatibus', '1992-06-20', 7, 1, NULL, NULL),
(82, 'Quibusdam dolorum similique.', 'Repellendus pariatur amet reiciendis nihil. Voluptatem quasi nemo porro odit eum. Voluptate et cum deleniti incidunt libero maxime est possimus. Quod fuga dolores quia soluta ducimus.', 'harum', '2016-08-30', 5, 2, NULL, NULL),
(83, 'Voluptatem nobis aut.', 'Ut unde quas cum sunt cupiditate id. Dicta sequi molestias distinctio facilis. Officiis quisquam molestias eveniet quisquam rerum. Illum rerum minus consequuntur asperiores aspernatur. Sint sint dolorum totam est dolores dolores.', 'harum', '2017-06-23', 6, 2, NULL, NULL),
(84, 'Alias quidem dolorum.', 'Deleniti fuga aut hic accusantium praesentium quos. Praesentium nobis quod veniam voluptatem eius repellat sequi incidunt. Amet tempora dolores totam officiis. Praesentium aut et id qui velit nisi.', 'explicabo rerum', '1993-10-09', 4, 2, NULL, NULL),
(85, 'Officia perspiciatis dolores enim enim.', 'Amet hic quas vel perspiciatis. Odio reprehenderit est modi nemo eos voluptate. Tempora qui neque ut rem deleniti.', 'assumenda', '1973-08-09', 6, 4, NULL, NULL),
(86, 'Sint voluptas ea consectetur.', 'Quos dolores placeat maiores enim quia ea. Aspernatur cumque et soluta. Sunt et voluptas quaerat nihil veniam qui. Consequatur labore laborum facere repellendus deserunt perferendis sed.', 'expedita et', '1993-04-28', 6, 2, NULL, NULL),
(87, 'Velit et id qui quaerat totam.', 'Qui ea doloremque ut voluptate deserunt. Similique tempore provident non reiciendis qui repellat aliquid. Voluptatem rerum corrupti a nulla voluptate quo necessitatibus. Debitis autem ea aliquam assumenda.', 'esse facilis', '1975-07-30', 11, 4, NULL, NULL),
(88, 'Asperiores ea amet molestiae ut.', 'Modi id quis dicta doloribus dolorum ut dolor. Mollitia molestiae iusto magni nostrum expedita et molestiae expedita. In suscipit atque molestias eos aperiam et enim. Doloremque perferendis doloribus molestias aliquid architecto occaecati tenetur.', 'ex quas', '1981-11-12', 7, 2, NULL, NULL),
(89, 'Placeat aut natus.', 'Incidunt fugit officia nesciunt omnis harum. Dolorem sit id sint quidem sequi. Debitis molestiae sint repellat at perferendis quam autem. Sunt vero esse aperiam soluta.', 'inventore molestiae', '2011-04-15', 3, 1, NULL, NULL),
(90, 'Necessitatibus repellendus quae voluptatem.', 'Dolorem est consequuntur quod repudiandae. Et dolore eius sed. Consequatur et harum non qui exercitationem. Vel culpa minima unde nostrum voluptatem porro. Ipsa nesciunt aperiam quo quas eos.', 'vel quo', '1987-10-26', 6, 3, NULL, NULL),
(91, 'Asperiores quis ea.', 'Optio nobis commodi laboriosam omnis consequatur tempora. Rem tempore quo sit itaque laborum aspernatur dolores.', 'sed', '2013-03-17', 2, 2, NULL, NULL),
(92, 'Sapiente ut quo consequuntur.', 'Expedita vel ratione quia assumenda dolorem. Illo odit est sequi at corporis sint. Aut totam id dolorem omnis. Voluptatum error nemo quia sed quos.', 'impedit', '2014-10-05', 7, 2, NULL, NULL),
(93, 'Omnis quo aut harum reiciendis.', 'Consequatur veniam quod ea aspernatur est ut ea. Aspernatur libero tempora totam ut asperiores eos nam. Doloribus eos rem omnis et non.', 'ut', '1985-03-11', 9, 4, NULL, NULL),
(94, 'Vel odit enim animi.', 'Dicta suscipit voluptatem consequatur. Dicta dolor culpa provident blanditiis ratione ut aut. Iste totam aut quidem itaque. Tempora amet praesentium est quae voluptate.', 'enim', '1992-02-05', 9, 3, NULL, NULL),
(95, 'Et voluptatem est dignissimos.', 'Tempora reprehenderit officiis quaerat amet illo eligendi delectus. Voluptas voluptas earum omnis delectus molestias saepe possimus modi. Molestias suscipit sunt voluptas nulla vitae non.', 'corporis', '1993-12-22', 8, 1, NULL, NULL),
(96, 'Eos itaque a autem.', 'Ut quae corporis sit reiciendis necessitatibus dolor nobis. Consequuntur sit mollitia sit dolores harum dolorem cum. Dolores iusto natus nobis quis praesentium necessitatibus. Ut cum doloremque animi quo.', 'ut corrupti', '1982-08-02', 9, 2, NULL, NULL),
(97, 'Et dignissimos qui in quod.', 'Velit doloremque ab enim facere quo veritatis adipisci. Est amet dolor perspiciatis eos. Reiciendis rerum quibusdam est dolores. Neque expedita tenetur laudantium autem quibusdam commodi necessitatibus odio. Quis dolores repellat et quia.', 'aspernatur aut', '2009-07-14', 3, 2, NULL, NULL),
(98, 'Nemo sed aspernatur repudiandae non.', 'Modi aut beatae reiciendis. Culpa voluptatum vero voluptatem labore nihil aliquid. Amet mollitia omnis id accusantium dolorem. Aut atque quas accusamus veniam voluptatem praesentium.', 'omnis', '1982-09-13', 2, 1, NULL, NULL),
(99, 'Et inventore sit consequuntur.', 'Inventore aut placeat sit inventore consequatur eum qui. Iste sunt similique consequatur esse sed nam. Sint minus neque fuga amet quia. Dolorem porro et sed eveniet aut.', 'quas', '1979-10-25', 2, 3, NULL, NULL),
(100, 'Nesciunt voluptatem fugit.', 'Repellendus saepe minus commodi eaque. Aut sed impedit culpa qui id est. Et aut provident autem possimus repudiandae et. Sint quo nesciunt laborum.', 'aperiam enim', '2012-11-18', 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-01-06 02:15:53', '2020-01-06 02:15:53'),
(2, 'user', 'web', '2020-01-06 02:15:53', '2020-01-06 02:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_hierarchy`
--

CREATE TABLE `role_hierarchy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `hierarchy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_hierarchy`
--

INSERT INTO `role_hierarchy` (`id`, `role_id`, `hierarchy`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `class`) VALUES
(1, 'ongoing', 'badge badge-pill badge-primary'),
(2, 'stopped', 'badge badge-pill badge-secondary'),
(3, 'completed', 'badge badge-pill badge-success'),
(4, 'expired', 'badge badge-pill badge-warning');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuroles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `menuroles`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@admin.com', '2020-01-06 02:15:54', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user,admin', 'TnjibMnHY72jaauJyDd0rPJaYt8AT8i72ngqqks7Wa8UKTufSAnPm3IVrUhu', '2020-01-06 02:15:54', '2020-01-06 02:15:54', NULL),
(2, 'Ms. Tiana Mohr', 'ernest89@example.com', '2020-01-06 02:15:54', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'nDmkCJKqSn', '2020-01-06 02:15:54', '2020-01-06 02:15:54', NULL),
(3, 'Providenci Tromp', 'mccullough.jermaine@example.com', '2020-01-06 02:15:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'FbAGK8fxig', '2020-01-06 02:15:55', '2020-01-06 02:15:55', NULL),
(4, 'Collin Bode', 'elenora.durgan@example.com', '2020-01-06 02:15:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'C4GRK3slKC', '2020-01-06 02:15:55', '2020-01-06 02:15:55', NULL),
(5, 'Kari Willms', 'miguel.hoppe@example.net', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', '7Kw3v23qbM', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(6, 'Novella Denesik PhD', 'angelo.kiehn@example.org', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'BOgZASJjL0', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(7, 'Jayde Emard', 'ncassin@example.net', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'mJoWk7hhsR', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(8, 'Emmie Grant', 'emil82@example.net', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'kNR38JBK9F', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(9, 'Aileen Schuppe', 'chester01@example.org', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'RZo3vu7VvX', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(10, 'Jennie Hahn', 'reva.pfeffer@example.org', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'hOMeHBq37u', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(11, 'Adriana Raynor', 'drolfson@example.net', '2020-01-06 02:15:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'sXyvOCWvnP', '2020-01-06 02:15:56', '2020-01-06 02:15:56', NULL),
(12, 'much ramdan', 'muchramdan123@gmail.com', NULL, '$2y$10$CsIprwPYkeLo9gp6PRB3vOHmgNBS9pKk9QMMfYnxqpEJR8lONxMj6', 'user', NULL, '2020-01-06 02:20:19', '2020-01-06 02:20:19', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menulist`
--
ALTER TABLE `menulist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menulist`
--
ALTER TABLE `menulist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `menu_role`
--
ALTER TABLE `menu_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
